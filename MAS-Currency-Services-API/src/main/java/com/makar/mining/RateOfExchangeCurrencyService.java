package com.makar.mining;



public interface RateOfExchangeCurrencyService
{
    String getRateOfExchange(String currencyCodeFrom, String StringCurrencyCodeTo);
}
