package com.makar.mining;



import java.util.List;

public interface RetrieveCurrencyService<T> {
    T retrieveCurrency(String code);
    List<T> retrieveAllCurrencies();
}
