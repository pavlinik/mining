package com.makar.mining;

import java.util.List;

public interface UpdateCurrencyService<T>
{
    T save(T currency);
    T update(T currency);
    T remove(T currency);
    List<T> save(List<T> currencies);
    List<T> remove(List<T> currencies);
    List<T> update(List<T> currencies);
}
