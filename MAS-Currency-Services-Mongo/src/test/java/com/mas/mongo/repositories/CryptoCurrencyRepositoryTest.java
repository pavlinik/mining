package com.mas.mongo.repositories;



import com.mas.models.Currency;
import com.mas.mongo.models.CurrencyMongoPersistentEntity;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:config/mongo-config.xml")
public class CryptoCurrencyRepositoryTest {
    private static final String LOCALHOST = "127.0.0.1";
    private static final int MONGO_TEST_PORT = 27017;

    private static MongodExecutable mongodExecutable;


    @Autowired
    @Qualifier("cryptoCurrencyRepository")
    private CryptoCurrencyRepository cryptoCurrencyRepository;


    @BeforeClass
    public static void initializeDB() throws IOException
    {
        MongodStarter starter = MongodStarter.getDefaultInstance();
        IMongodConfig mongodConfig = new MongodConfigBuilder()
            .version(Version.Main.PRODUCTION)
            .net(new Net(LOCALHOST, MONGO_TEST_PORT, Network.localhostIsIPv6()))
            .build();
            mongodExecutable = starter.prepare(mongodConfig);
            mongodExecutable.start();
    }

    @AfterClass
    public static void stopDB() throws IOException
    {
        mongodExecutable.stop();
    }



    @Test
    public void testSave(){

        CurrencyMongoPersistentEntity currencyExp = new CurrencyMongoPersistentEntity();
        currencyExp.setId(new Long("111111"));
        currencyExp.setCoinName("Digibyte");
        currencyExp.setFullName("Digibyte");
        currencyExp.setName("DGB");


        cryptoCurrencyRepository.save(currencyExp);
        Optional<CurrencyMongoPersistentEntity> currencyActOpt = cryptoCurrencyRepository
                .findById(new Long("111111"));
        Currency currencyAct = currencyActOpt.get();
        Assert.assertEquals(currencyExp, currencyAct);
    }

    @Test
    public void testDelete(){
        CurrencyMongoPersistentEntity currencyExp = new CurrencyMongoPersistentEntity();
        currencyExp.setId(new Long("111111"));
        currencyExp.setCoinName("Digibyte");
        currencyExp.setFullName("Digibyte");
        currencyExp.setName("ASC");
        currencyExp.setAlgorithm("Neoscrypt");
        currencyExp.setProofType("Pos");
        boolean isPresent;

        CurrencyMongoPersistentEntity currencyAct = cryptoCurrencyRepository.save(currencyExp);
        isPresent = cryptoCurrencyRepository.findById(new Long("111111")).isPresent();
        Assert.assertTrue(isPresent);

        cryptoCurrencyRepository.delete(currencyExp);
        isPresent = cryptoCurrencyRepository
                .findById(new Long("111111")).isPresent();
        Assert.assertFalse(isPresent);
    }

}
