package com.mas.mongo.converters;


import com.mas.mongo.models.CurrencyMongoPersistentEntity;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;

public class CurrencyWriteConverter implements Converter<CurrencyMongoPersistentEntity, Document> {
    @Nullable
    @Override
    public Document convert(CurrencyMongoPersistentEntity currency) {
            Document document = new Document();
            document.put("_id", currency.getId());
            document.put("name", currency.getName());
            document.put("coinName", currency.getCoinName());
            document.put("fullName", currency.getFullName());
            document.put("algorithm", currency.getAlgorithm());
            document.put("proofType", currency.getProofType());
            return document;
    }
}
