package com.mas.mongo.converters;
import com.mas.mongo.models.CurrencyMongoPersistentEntity;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;

public class CurrencyReadConverter implements Converter<Document,CurrencyMongoPersistentEntity> {
    @Nullable
    @Override
    public CurrencyMongoPersistentEntity convert(Document source) {
        CurrencyMongoPersistentEntity currency = new CurrencyMongoPersistentEntity();
        currency.setId((Long) source.get("_id"));
        currency.setName((String)source.get("name"));
        currency.setCoinName((String)source.get("coinName"));
        currency.setFullName((String)source.get("fullName"));
        currency.setAlgorithm((String)source.get("algorithm"));
        currency.setProofType((String)source.get("proofType"));
        return currency;
    }
}
