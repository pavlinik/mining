package com.mas.mongo.models;

import com.mas.models.Currency;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "currencies")
public class CurrencyMongoPersistentEntity extends Currency
{
    /** The internal idProperty, this is used in other calls */
    private Long id;


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
