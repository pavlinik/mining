package com.mas.mongo.services;

import com.makar.mining.RetrieveCurrencyService;

import com.mas.mongo.repositories.CryptoCurrencyRepository;
import com.mas.mongo.models.CurrencyMongoPersistentEntity;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MongoCryptoCurrencyRetrieveService implements RetrieveCurrencyService {

    @Autowired
    private CryptoCurrencyRepository cryptoCurrencyRepository;

    public MongoCryptoCurrencyRetrieveService(CryptoCurrencyRepository cryptoCurrencyRepository) {
        this.cryptoCurrencyRepository = cryptoCurrencyRepository;
    }

    public CryptoCurrencyRepository getCryptoCurrencyRepository() {
        return cryptoCurrencyRepository;
    }

    public void setCryptoCurrencyRepository(CryptoCurrencyRepository cryptoCurrencyRepository) {
        this.cryptoCurrencyRepository = cryptoCurrencyRepository;
    }

    @Override
    public CurrencyMongoPersistentEntity retrieveCurrency(String code) {
        List<CurrencyMongoPersistentEntity> currencyList = retrieveAllCurrencies();
        CurrencyMongoPersistentEntity currencyRes = currencyList.stream()
                .filter(currency -> currency.getName().equalsIgnoreCase(code))
                .findFirst().orElse(null);
        return currencyRes;
    }

    @Override
    public List<CurrencyMongoPersistentEntity> retrieveAllCurrencies() {
        Iterable<CurrencyMongoPersistentEntity> currencyIterable = cryptoCurrencyRepository.findAll();
        Stream<CurrencyMongoPersistentEntity> currencyStream = StreamSupport
                .stream(currencyIterable.spliterator(), false);
        List<CurrencyMongoPersistentEntity> result = currencyStream.collect(Collectors.toList());
        return result;
    }


}
