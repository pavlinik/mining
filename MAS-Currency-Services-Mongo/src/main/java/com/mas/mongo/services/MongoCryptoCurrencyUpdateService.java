package com.mas.mongo.services;

import com.makar.mining.UpdateCurrencyService;
import com.mas.mongo.repositories.CryptoCurrencyRepository;
import com.mas.mongo.models.CurrencyMongoPersistentEntity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MongoCryptoCurrencyUpdateService implements UpdateCurrencyService<CurrencyMongoPersistentEntity> {

    private CryptoCurrencyRepository cryptoCurrencyRepository;

    public MongoCryptoCurrencyUpdateService(CryptoCurrencyRepository cryptoCurrencyRepository) {
        this.cryptoCurrencyRepository = cryptoCurrencyRepository;
    }

    public CryptoCurrencyRepository getCryptoCurrencyRepository() {
        return cryptoCurrencyRepository;
    }

    public void setCryptoCurrencyRepository(CryptoCurrencyRepository cryptoCurrencyRepository) {
        this.cryptoCurrencyRepository = cryptoCurrencyRepository;
    }


    @Override
    public CurrencyMongoPersistentEntity save(CurrencyMongoPersistentEntity currency) {
        return cryptoCurrencyRepository.save(currency);
    }

    @Override
    public CurrencyMongoPersistentEntity update(CurrencyMongoPersistentEntity currency) {
        return cryptoCurrencyRepository.save(currency);
    }

    @Override
    public CurrencyMongoPersistentEntity remove(CurrencyMongoPersistentEntity currency) {
        cryptoCurrencyRepository.delete(currency);
        return currency;
    }

    @Override
    public List<CurrencyMongoPersistentEntity> save(List<CurrencyMongoPersistentEntity> currencies) {
        Iterable<CurrencyMongoPersistentEntity> iterable = cryptoCurrencyRepository.saveAll(currencies);
        List<CurrencyMongoPersistentEntity> currencyList = StreamSupport.stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
        return currencyList;
    }

    @Override
    public List<CurrencyMongoPersistentEntity> remove(List<CurrencyMongoPersistentEntity> currencies) {
        return null;
    }

    @Override
    public List<CurrencyMongoPersistentEntity> update(List<CurrencyMongoPersistentEntity> currencies) {
        return null;
    }
}
