package com.mas.mongo.repositories;


import com.mas.mongo.models.CurrencyMongoPersistentEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CryptoCurrencyRepository extends MongoRepository<CurrencyMongoPersistentEntity, Long>
{

}
