package com.makar.mining.mongobee.configuration;

import com.github.mongobee.Mongobee;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Makaranka_Andrey on 10/23/2017.
 */
@Configuration
public class MainConfiguration {
    @Bean
    public Mongobee mongobee(){
        Mongobee runner = new Mongobee("mongodb://127.0.0.1:27017/mining");
        runner.setDbName("mining");         // host must be set if not set in URI
        runner.setChangeLogsScanPackage(
                "com.makar.mining.mongobee.changelogs"); // the package to be scanned for changesets
        runner.setEnabled(false);
        return runner;
    }
}
