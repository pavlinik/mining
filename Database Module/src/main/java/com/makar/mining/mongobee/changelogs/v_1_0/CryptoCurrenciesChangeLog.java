package com.makar.mining.mongobee.changelogs.v_1_0;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by Makaranka_Andrey on 10/23/2017.
 */
@ChangeLog(order = "0001")
public class CryptoCurrenciesChangeLog {
    @ChangeSet(order = "001", id = "createCurrenciesCollection_v_1_0_001", author = "Andrey Makaranaka", runAlways = true)
    public void createCurrenciesCollection(MongoDatabase db) throws IOException {
        MongoCollection<Document> mycollection = db.getCollection("currencies");
        mycollection.deleteMany(new Document());

      //
        Resource resource = new ClassPathResource("changelogs/v_1_0/currencies.json");
        byte[] fileBytes = Files.readAllBytes(resource.getFile().toPath());
        String content = new String (fileBytes, Charset.forName("UTF-8"));
        Document doc = Document.parse(content);

        Document data = (Document) doc.get("Data");
        List<Document> currencies = new ArrayList<>();
        data.forEach((key, value) -> currencies.add(new Document(key, value)));

        mycollection.insertMany(currencies);
    }

}
