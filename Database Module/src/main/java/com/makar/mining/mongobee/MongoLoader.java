package com.makar.mining.mongobee;

import com.github.mongobee.Mongobee;
import com.github.mongobee.exception.MongobeeException;
import com.makar.mining.mongobee.configuration.MainConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


/**
 * Created by Makaranka_Andrey on 10/23/2017.
 */
public class MongoLoader {
    public static void main(String[] args) throws MongobeeException {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(MainConfiguration.class);

        Mongobee mongobee = ctx.getBean(Mongobee.class);
        mongobee.setEnabled(true);
        mongobee.execute();

    }
}
