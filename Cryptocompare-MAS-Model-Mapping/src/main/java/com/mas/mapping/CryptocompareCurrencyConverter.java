package com.mas.mapping;

import com.mas.converters.Converter;
import com.mas.models.cryptocompare.Currency;

public class CryptocompareCurrencyConverter implements Converter<Currency, com.mas.models.Currency>
{
    @Override
    public com.mas.models.Currency convert(Currency source)
    {
        com.mas.models.Currency dest = new com.mas.models.Currency();
        dest.setName(source.getName());
        dest.setCoinName(source.getCoinName());
        dest.setFullName(source.getFullName());
        dest.setAlgorithm(source.getAlgorithm());
        dest.setProofType(dest.getProofType());
        return dest;
    }
}
