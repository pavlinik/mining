package com.mas.models


import groovy.transform.Canonical

@Canonical
class Currency implements Serializable{
    /** The symbol*/
    String name
    /** The name */
    String coinName
    /** A combination of the name and the symbol */
    String fullName
    /** The algorithm of the cryptocurrency */
    String algorithm
    /** The proof type of the cryptocurrency */
    String proofType
}