package com.makar.mining.cryptocampare.repositories;


import org.springframework.data.repository.CrudRepository;
import com.mas.models.cryptocompare.Currency;


public interface CurrencyRepository extends CrudRepository<Currency, Long> {

}
