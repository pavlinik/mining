package com.makar.mining.cryptocampare.repositories;


import com.mas.models.cryptocompare.CurrenciesResponse;
import com.mas.models.cryptocompare.Currency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;


public class RESTCurrencyRepository implements CurrencyRepository {
    private static final Logger log = LoggerFactory.getLogger(RESTCurrencyRepository.class);

    @Override
    @Deprecated
    public Currency save(Currency currency) {
        try {
            throw new NoSuchMethodException("Host does't allow to save currencies");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    @Deprecated
    public <S extends Currency> Iterable<S> save(Iterable<S> iterable) {
        try {
            throw new NoSuchMethodException("Host does't allow to save currencies");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @Deprecated
    public Currency findOne(Long aLong) {
        try {
            throw new NoSuchMethodException("Host does't allow to save currencies");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return  null;
    }

    @Override
    @Deprecated
    public boolean exists(Long aLong) {
        try {
            throw new NoSuchMethodException("Host does't allow to save currencies");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Iterable<Currency> findAll() {
        RestTemplate restTemplate = new RestTemplate();
        CurrenciesResponse currenciesResponse = restTemplate
                .getForObject("https://www.cryptocompare.com/api/data/coinlist/", CurrenciesResponse.class);
        log.info(currenciesResponse.toString());
        return currenciesResponse.getCurrencyMap().values();
    }

    @Override
    @Deprecated
    public Iterable<Currency> findAll(Iterable<Long> iterable) {
        try {
            throw new NoSuchMethodException("Host does't allow to save currencies");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    @Deprecated
    public long count() {
        try {
            throw new NoSuchMethodException("Host does't allow to save currencies");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    @Deprecated
    public void delete(Long aLong) {
        try {
            throw new NoSuchMethodException("Host does't allow to save currencies");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Deprecated
    public void delete(Currency currency) {
        try {
            throw new NoSuchMethodException("Host does't allow to save currencies");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Deprecated
    public void delete(Iterable<? extends Currency> iterable) {
        try {
            throw new NoSuchMethodException("Host does't allow to save currencies");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Deprecated
    public void deleteAll() {
        try {
            throw new NoSuchMethodException("Host does't allow to save currencies");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }


}
