package com.makar.mining.cryptocampare.services;


import com.makar.mining.RetrieveCurrencyService;
import com.mas.models.cryptocompare.Currency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.makar.mining.cryptocampare.repositories.CurrencyRepository;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CryptocompareRetrieveCurrencyService implements RetrieveCurrencyService<Currency> {
    private static final Logger log = LoggerFactory.getLogger(CryptocompareRetrieveCurrencyService.class);

    private CurrencyRepository currencyRepository;

    public CryptocompareRetrieveCurrencyService(){}

    public CryptocompareRetrieveCurrencyService(CurrencyRepository currencyRepository)
    {
        this.currencyRepository = currencyRepository;
    }

    public CurrencyRepository getRetrieveCurrencyService()
    {
        return currencyRepository;
    }

    public void setRetrieveCurrencyService(CurrencyRepository currencyRepository)
    {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public Currency retrieveCurrency(String code)
    {
        List<Currency> currencyList = retrieveAllCurrencies();
        Currency currencyRes = currencyList.stream()
                .filter(currency -> currency.getName().equalsIgnoreCase(code))
                .findFirst().orElse(null);
        return currencyRes;
    }

    @Override
    public List<Currency> retrieveAllCurrencies() {
        Iterable<Currency> currencyIterable = currencyRepository.findAll();
        Stream<Currency> currencyStream = StreamSupport
                .stream(currencyIterable.spliterator(), false);
        List<Currency> result = currencyStream.collect(Collectors.toList());
        return result;
    }


}
