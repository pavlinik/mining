package com.mas.models.cryptocompare

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.Canonical


@Canonical
@JsonIgnoreProperties(ignoreUnknown = true)
class Currency {
    /** The symbol*/
    @JsonProperty("Name")
    String name
    /** The internal id, this is used in other calls */
    @JsonProperty("Id")
    int id
    /** The url of the coin on cryptocompare */
    @JsonProperty("Url")
    String url
    /** The logo image of the coin */
    @JsonProperty("ImageUrl")
    String imageUrl
    /** The name */
    @JsonProperty("CoinName")
    String coinName
    /** A combination of the name and the symbol */
    @JsonProperty("FullName")
    String fullName
    /** The algorithm of the cryptocurrency */
    @JsonProperty("Algorithm")
    String algorithm
    /** The proof type of the cryptocurrency */
    @JsonProperty("ProofType")
    String proofType
    /** The order we rank the coin inside our internal system */
    @JsonProperty("SortOrder")
    String sortOrder
}