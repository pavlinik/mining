package com.mas.models.cryptocompare

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.Canonical

@Canonical
@JsonIgnoreProperties(ignoreUnknown = true)
class CurrenciesResponse
{
    @JsonProperty("Response")
    String response
    @JsonProperty("Message")
    String message
    @JsonProperty("BaseImageUrl")
    String baseImageUrl
    @JsonProperty("BaseLinkUrl")
    String baseLinkUrl
    @JsonProperty("Data")
    Map<String, Currency> currencyMap
    @JsonProperty("Type")
    String type
}
