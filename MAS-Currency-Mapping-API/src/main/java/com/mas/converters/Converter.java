package com.mas.converters;

public interface Converter<S,T>
{
    T convert(S source);
}
